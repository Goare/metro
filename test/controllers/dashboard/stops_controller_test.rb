require 'test_helper'

class Dashboard::StopsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_stops_index_url
    assert_response :success
  end

  test "should get create" do
    get dashboard_stops_create_url
    assert_response :success
  end

  test "should get show" do
    get dashboard_stops_show_url
    assert_response :success
  end

end
