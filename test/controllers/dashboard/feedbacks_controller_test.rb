require 'test_helper'

class Dashboard::FeedbacksControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get dashboard_feedbacks_index_url
    assert_response :success
  end

end
