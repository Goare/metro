Rails.application.routes.draw do
  resources :files, only: [:index, :create]

  namespace :dashboard do
    resources :lines
    resources :stops, only: [:index, :create]
    get 'stops/:name' => 'stops#show', as: 'stop_name'
    resources :stops, only: [:index, :create, :show]
    resources :toilets, only: [:index, :create, :update, :destroy]

    resources :feedbacks, only: [:index, :destroy]
  end

  resources :logs, only: [:index, :create]

  get 'dashboard' => 'dashboard#index'
  get 'about' => 'home#about', as: 'about'
  root 'home#index'

  resources :sessions, only: [:new, :create, :destroy]
  get 'login' => 'sessions#new'
  # resources :users, only: [:new, :create]
  # get 'register' => 'users#new'

  get 'lines/:name' => 'lines#show', as: 'line_name'
  resources :lines, only: [:index]

  get 'stops/:name' => 'stops#show', as: 'stop_name'

  resources :feedbacks, only: [:new, :create]

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
