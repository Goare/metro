json.array! @files do |file|
  json.id file.id
  json.location file.file.url
end