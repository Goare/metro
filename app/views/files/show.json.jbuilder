if @file
  json.location @file.file.url
end

if @files
  json.array! @files do |file|
    json.location file.file.url
  end
end

