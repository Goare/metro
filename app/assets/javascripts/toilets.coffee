# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  tinymce.init({
#    theme_url: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.5/themes/modern/theme.min.js'
#    skin_url: 'https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.5/skins/lightgray'
    selector: 'textarea.toilet-textarea'
#    document_base_url: "https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.5.5/",
#    relative_urls: true,
    plugins: 'image code autolink preview paste'
    toolbar: "undo redo | link image | code preview"
    paste_as_text: true
    image_title: true
    automatic_uploads: true
    file_picker_types: "image"
    # add upload into image upload file picker form
    file_picker_callback: (cb, value, meta)->
      input = document.createElement("input")
      input.setAttribute('type', 'file')
      input.setAttribute('name', 'files[]')
      input.setAttribute('accept', 'image/*')

      input.onchange = ()->
        file = this.files[0]

        reader = new FileReader()
        reader.readAsDataURL(file)
        reader.onload = ()->
          id = 'blobid' + (new Date()).getTime()
          blobCache = tinymce.activeEditor.editorUpload.blobCache
          blobInfo = blobCache.create(id, file, reader.result)
          blobCache.add(blobInfo)

          cb(blobInfo.blobUri(), {title: file.name})

      input.click()
    images_upload_url: "/files"
    language: 'zh_CN'
  });