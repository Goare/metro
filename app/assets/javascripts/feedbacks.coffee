# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$ ->
  $('#feedback-rucaptcha').on 'click', ->
    $(this).attr('src', '/rucaptcha?'+(new Date()).valueOf())

  $feedbackNewFormAction = $('#feedback-new-form')
  $feedbackNewFormAction.attr('action', $feedbackNewFormAction.attr('action') + window.location.search)