module SessionsHelper
  def log_in
    session[:current_user_id] = @user.id
  end

  def current_user
    @current_user = User.find_by(id: session[:current_user_id])
  end

  def logged_in?
    !current_user.nil?
  end

  def log_out
    session.delete(:current_user_id)
    @current_user = nil
  end
end
