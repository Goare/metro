module StopsHelper
  SPECIAL_STOPS = %w(浦电路)

  def special_stop?(stop_name)
    SPECIAL_STOPS.include? stop_name
  end
end
