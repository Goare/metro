module ApplicationHelper
  def page_title(title = "")
    t = "上海地铁站内厕所索引"
    title.blank? ? t : "#{title} - #{t}"
  end

  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end
end
