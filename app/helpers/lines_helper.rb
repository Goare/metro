module LinesHelper
  include StopsHelper
  def say_hi
    "Hello world"
  end

  def get_stop_name(stop)
    @stop_name = stop.name
  end

  def get_stops_from_stop_name(stop_name)
    @stops = Stop.where(name: stop_name)

    # 404
    not_found if @stops.empty?
  end

  def lines_of_stop(stop_name)
    get_stops_from_stop_name(stop_name)
    @lines = @stops.map {|stop| stop.line }
  end

  def toilets_of_stop(stop_name)
    get_stops_from_stop_name(stop_name)
    @toilets = @stops.map {|stop| stop.toilets}.compact.flatten
  end

  def stop_has_toilets?(stop)
    get_stop_name(stop)
    toilets_of_stop(@stop_name).empty?
  end

  def stop_has_toilets_at_other_line?(stop)
    get_stop_name(stop)
    all_toilets = toilets_of_stop(@stop_name)
    stop_toilets = stop.toilets

    if special_stop?(@stop_name)
      # SPECIAL_STOPS 不通，若通便是换乘站，所以一直返回 true
      true
    else
      (all_toilets - stop_toilets).empty?
    end
  end

  # 可换乘的line
  def exchangeable_lines(stop)
    get_stop_name(stop)
    if special_stop?(@stop_name)
      []
    else
      lines_of_stop(@stop_name) - [stop.line]
    end

    # return @lines, array
  end

  def exchange_stop?(stop)
    get_stop_name(stop)
    if special_stop?(@stop_name)
      false
    else
      (lines_of_stop(@stop_name).count > 1)
    end
  end

end
