class FilesController < ApplicationController
  skip_before_action :verify_authenticity_token
  def index
    @files = Attachment.all
  end

  # upload single file using name: file
  # upload multiple files using name: files[]
  def create
    unless params[:file].nil?
      @file = create_file(params[:file])
    end

    unless params[:files].nil?
      @files = []
      params[:files].each do |file|
        @files << create_file(file)
      end
    end

    render :show
  end

  def show
  end

  private
    def create_file(raw_file)
      Attachment.create(file: raw_file)
    end
end
