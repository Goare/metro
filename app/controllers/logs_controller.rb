class LogsController < ApplicationController
  before_action :require_log_in, only: [:create]

  def index
    @logs = Log.all.order(created_at: :desc)
  end

  def create
    @log = Log.new(params.require(:log).permit(:title, :content))
    @log.user = helpers.current_user

    if @log.save
      redirect_to logs_path
    end
  end
end
