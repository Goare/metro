class UsersController < ApplicationController
  def new
    @user = User.new
  end

  def create
    @user = User.new(params.require(:user).permit(:name, :password))

    if @user.save
      helpers.log_in

      flash[:success] = "Register successfully"
      redirect_to dashboard_path
    else
      flash[:warning] = "Register fails"
      redirect_to register_path
    end
  end
end
