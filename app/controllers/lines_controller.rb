class LinesController < ApplicationController
  def index
    @lines = Line.all
  end


  def show
    line_name = params[:name]
    @line = Line.find_by(name: line_name)

    helpers.not_found unless @line

    @lines = Line.all
    @stops = @line.stops
    @stop = Stop.new
  end
end
