class FeedbacksController < ApplicationController
  def new
  end

  def create
    title = nil || params[:title]
    @feedback = Feedback.new(params[:feedback].permit(:contact, :content))
    @feedback.title = title

    if verify_rucaptcha?(@feedback) && @feedback.save
      flash[:success] = '谢谢您的反馈'
    else
      flash[:warning] = '反馈提交失败'
    end

    redirect_to new_feedback_path
  end
end
