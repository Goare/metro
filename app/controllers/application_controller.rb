class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def require_log_in
    unless helpers.logged_in?
      flash[:warning] = "please log in first"

      session[:redirect_to] = request.url
      redirect_to login_path
    end
  end
end
