class Dashboard::FeedbacksController < DashboardController
  def index
    @feedbacks = Feedback.all.order(created_at: :desc)
  end

  def destroy
    @feedback = Feedback.find(params[:id])

    if @feedback.destroy
      flash[:success] = 'feedback destroy successfully'
      redirect_to dashboard_feedbacks_path
    end
  end
end
