class Dashboard::StopsController < DashboardController
  def index
  end

  def create
    line_id = params[:line][:id]

    @stop = Stop.new(params.require(:stop).permit(:name))
    @stop.line = Line.find(line_id)

    if @stop.save
      redirect_to dashboard_line_path(line_id)
    end
  end

  def show
    @stop_name = params[:name]
    @lines = helpers.lines_of_stop(@stop_name)
    @toilets = helpers.toilets_of_stop(@stop_name)
  end
end
