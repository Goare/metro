class Dashboard::ToiletsController < DashboardController
  def index
    @toilets = Toilet.all
  end

  def create
    stop_name = params[:stop_name]
    line_id = params[:line_id]
    stop_id = Line.find_by(id: line_id).stops.select {|stop| stop.name.eql? stop_name}.first.id

    @toilet = Toilet.new(
        description: params[:toilet][:description],
        stop_id: stop_id,
        line_id: params[:line_id]
    )

    if @toilet.save
      redirect_to :back
    end
  end

  def destroy
    @toilet = Toilet.find(params[:id])
    if @toilet.destroy
      flash[:success] = "destroy #{@toilet.description} success"
      redirect_to dashboard_toilets_path
    end
  end
end
