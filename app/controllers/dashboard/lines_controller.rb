class Dashboard::LinesController < DashboardController
  def index
    @lines = Line.all
  end

  def create
    @line = Line.new(params.require(:line).permit(:name))

    if @line.save
      redirect_to dashboard_lines_path
    end
  end

  def show
    line_id = params[:id]
    @line = Line.find_by(id: line_id)
    @lines = Line.all

    @stops = @line.stops
  end

  def edit
  end

  def update
  end

  def destroy
    @line = Line.find(params[:id])

    if @line.destroy
      redirect_to dashboard_lines_path
    end
  end
end
