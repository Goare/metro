class StopsController < ApplicationController
  def index
    @stops = Stop.all
  end

  def show
    @stop_name = params[:name]
    @lines = helpers.lines_of_stop(@stop_name)
    @toilets = helpers.toilets_of_stop(@stop_name)
  end
end
