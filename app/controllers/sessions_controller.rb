class SessionsController < ApplicationController
  before_action :require_log_in, only: [:destroy]

  def new
    if helpers.logged_in?
      redirect_to dashboard_path
    end
  end

  def create
    redirect_to_path = session[:redirect_to]
    name = params[:user][:name]
    password = params[:user][:password]

    @user = User.find_by(name: name)

    if @user && @user.authenticate(password)
      helpers.log_in
      flash[:success] = 'u have logged in'

      if redirect_to_path.present?
        session.delete(:redirect_to)
        redirect_to redirect_to_path
      else
        redirect_to dashboard_path
      end

    else
      flash[:warning] = 'name or password wrong'
      redirect_to login_path
    end
  end

  def destroy
    helpers.log_out
    redirect_to login_path
  end
end
