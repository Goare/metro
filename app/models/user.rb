class User < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :email, format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create}, uniqueness: {case_sensitive: false}, allow_blank: true
  validates :password, presence: true, length: {minimum: 6}
  has_secure_password

  has_many :logs
end
