class Line < ApplicationRecord
  has_many :stops, dependent: :destroy
  has_many :toilets, dependent: :destroy
end
