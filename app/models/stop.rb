class Stop < ApplicationRecord
  belongs_to :line
  has_many :toilets, dependent: :destroy
end
